# ICMI2022 Knudsen et al Poster presentation

Hello there!
Thanks for stopping by and checking out our latest results :) For more information you can contact me at [christelle.knudsen\@inrae.fr](mailto:christelle.knudsen@inrae.fr){.email}

## Abstract

### Single cell RNA-sequencing: a tool to study the diversity of the *lamina propria* CD45<sup>+</sup> cells in the rabbit caecum

Rabbits are reliable animal models used in immunological research, in particular to study gut mucosal immunity.
The *lamina propria*, underlying the epithelium in the mucosa, is considered the primary effector site of the intestinal immune response and is composed of a heterogeneous immune cell population.
A detailed characterization of the diversity of mononuclear hematopoietic cells (CD45<sup>+</sup>) in the rabbit caecal *lamina propria* is lacking, even though this gut segment has a key role in the interaction with the gut microbiota.
The present study aimed to fill that void.
We isolated caecal *lamina propria* mononuclear cells by physical, chemical and enzymatic dissociation followed by a purification in a Percoll gradient.
Single live CD45<sup>+</sup> cells were sorted by flow cytometry (16% of total) and droplet-based single cell RNA-sequencing was performed.
Data were analyzed using the Cell Ranger software and the R package Seurat.
After applying quality filters, 4 064 cells were kept and the expression of 29 587 genes was detected.
12 cell clusters were identified, with the majority affiliated to B, T, mast and dendritic cells and macrophages based on the expression of canonical markers (*JCHAIN, RORA, KIT, NKG7, C1QB, CD14*).
Our method thus enabled us to clearly identify the main mononuclear immune cell types present in the *lamina propria* with little contamination with other cell types (e.g. fibroblast).
This study provides the first single cell phenotyping data on rabbit caecal mononuclear hematopoietic cells and offers great perspectives for the study of microbiota gut mucosal immunity regulations.

[Figure 1. UMAP projection of the scRNA-seq data of CD45+ rabbit caecal lamina propria cells. Each dot represents the transcriptome of a single cell, with color according to the identified cluster](Figure_resultats.png)

## Authors and acknowledgment

KNUDSEN Christelle<sup>1</sup>, MARTINS Frédéric<sup>2,3</sup>, CABAU Cédric<sup>1</sup>, ZAKAROFF-GIRARD Alexia<sup>2,4</sup>, RIANT Elodie<sup>2,4</sup>, GALLO Lise<sup>1</sup>, AYMARD Patrick<sup>1</sup>, COMBES Sylvie<sup>1</sup>, BEAUMONT Martin<sup>1</sup>

1 - GenPhySE, Université de Toulouse, INRAE, ENVT, F-31326, Castanet-Tolosan, France.

2 - Institut des Maladies Métaboliques et Cardiovasculaires (I2MC), INSERM UMR 1297, 31432 Toulouse, France.

3 - Plateforme GeT, Genotoul, 31100 Toulouse, France.

4 - Plateforme TRI, Genotoul, 31100 Toulouse, France.
